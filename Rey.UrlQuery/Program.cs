﻿using Rey.Exgaming.UrlQuery;
using System;
using System.Text;

namespace Rey.UrlQuery {
    class Program {
        static void Main(string[] args) {
            var chars = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var len = chars.Length;

            //! (name $eq kevin)
            //! $eq name kevin
            var exp = new UrlQueryParser().Parse("~eq-name-kevin");
            var str = exp.ToDebugString();

            //! ((name $eq kevin) $and (age $lt 24))
            //! $and $eq name kevin $lt age 24
            var exp2 = new UrlQueryParser().Parse("~and-~eq-name-kevin-~lt-age-24");
            var str2 = exp2.ToDebugString();

            //! (((name $eq kevin) $and (age $lt 24)) $or (height $gte 180))
            //! $or $and $eq name kevin $lt age 24 $gte height 180
            var exp3 = new UrlQueryParser().Parse("~or-~and-~eq-name-kevin-~lt-age-24-~gte-height-180");
            var str3 = exp3.ToDebugString();

            //! (((name $eq kevin) $and (age $lt 24)) $or ((weight $lte 70) $and (height $gte 180)))
            //! $or $and $eq name kevin $lt age 24 $and $lte weight 70 $gte height 180
            var exp4 = new UrlQueryParser().Parse("~or-~and-~eq-name-kevin-~lt-age-24-~and-~lte-weight-70-~gte-height-180");
            var str4 = exp4.ToDebugString();
        }
    }
}
