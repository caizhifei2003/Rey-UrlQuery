﻿using System;
using System.Linq;

namespace Rey.Exgaming.UrlQuery {
    public class OperatorBuilder {
        public char[] Chars { get; }

        public OperatorBuilder(char[] chars) {
            this.Chars = chars;
        }

        public QueryOperator Build(ref int index) {
            //! ~eq ~ne ~gt ~gte ~lt ~lte ~in ~nin
            var ch = this.Chars[index];
            if (ch != '~')
                return null;

            var i = index + 1;
            var symbol = QueryOperator.Symbols.All.ToList()
                .OrderByDescending(item => item.Length)
                .Where(item => {
                    if (i + item.Length >= this.Chars.Length)
                        return false;
                    return new string(this.Chars.Skip(i).Take(item.Length).ToArray()).Equals(item);
                }).FirstOrDefault();

            if (symbol != null) {
                index = i + symbol.Length;
                return QueryOperator.Create(symbol);
            }

            throw new InvalidOperationException("cannot find operator");
        }
    }
}
