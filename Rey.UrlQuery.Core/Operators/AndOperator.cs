﻿using Rey.Exgaming.UrlQuery.Expressions;

namespace Rey.Exgaming.UrlQuery.Operators {
    public class AndOperator : LogicalOperator {
        public AndOperator() : base(Symbols.And) { }
        public override LogicalExpression CreateExpression(QueryExpression exp1, QueryExpression exp2) {
            return new AndExpression(this, exp1, exp2);
        }
    }
}
