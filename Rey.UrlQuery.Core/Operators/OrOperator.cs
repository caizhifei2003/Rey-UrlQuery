﻿using Rey.Exgaming.UrlQuery.Expressions;

namespace Rey.Exgaming.UrlQuery.Operators {
    public class OrOperator : LogicalOperator {
        public OrOperator() : base(Symbols.Or) { }
        public override LogicalExpression CreateExpression(QueryExpression exp1, QueryExpression exp2) {
            return new OrExpression(this, exp1, exp2);
        }
    }
}
