﻿using Rey.Exgaming.UrlQuery.Expressions;

namespace Rey.Exgaming.UrlQuery.Operators {
    public class GreatThanEqualOperator : UniaryQueryOperator {
        public GreatThanEqualOperator() : base(Symbols.GreatThanEqual) { }
        public override QueryExpression CreateExpression(QueryOperand operand) {
            return new GreatThanEqualExpression(this, operand);
        }
    }
}
