﻿using Rey.Exgaming.UrlQuery.Expressions;

namespace Rey.Exgaming.UrlQuery.Operators {
    public class LessThanEqualOperator : UniaryQueryOperator {
        public LessThanEqualOperator() : base(Symbols.LessThanEqual) { }
        public override QueryExpression CreateExpression(QueryOperand operand) {
            return new LessThanEqualExpression(this, operand);
        }
    }
}
