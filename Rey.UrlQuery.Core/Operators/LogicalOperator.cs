﻿using Rey.Exgaming.UrlQuery.Expressions;

namespace Rey.Exgaming.UrlQuery.Operators {
    public abstract class LogicalOperator : QueryOperator {
        public LogicalOperator(string symbol) : base(symbol) {
        }

        public abstract LogicalExpression CreateExpression(QueryExpression exp1, QueryExpression exp2);
    }
}
