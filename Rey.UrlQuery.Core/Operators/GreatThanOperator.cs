﻿using Rey.Exgaming.UrlQuery.Expressions;

namespace Rey.Exgaming.UrlQuery.Operators {
    public class GreatThanOperator : UniaryQueryOperator {
        public GreatThanOperator() : base(Symbols.GreatThan) { }
        public override QueryExpression CreateExpression(QueryOperand operand) {
            return new GreatThanExpression(this, operand);
        }
    }
}
