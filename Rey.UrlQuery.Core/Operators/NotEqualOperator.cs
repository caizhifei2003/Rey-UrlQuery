﻿using Rey.Exgaming.UrlQuery.Expressions;

namespace Rey.Exgaming.UrlQuery.Operators {
    public class NotEqualOperator : UniaryQueryOperator {
        public NotEqualOperator() : base(Symbols.NotEqual) { }
        public override QueryExpression CreateExpression(QueryOperand operand) {
            return new NotEqualExpression(this, operand);
        }
    }
}
