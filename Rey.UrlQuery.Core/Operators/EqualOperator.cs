﻿using Rey.Exgaming.UrlQuery.Expressions;

namespace Rey.Exgaming.UrlQuery.Operators {
    public class EqualOperator : UniaryQueryOperator {
        public EqualOperator() : base(Symbols.Equal) { }
        public override QueryExpression CreateExpression(QueryOperand operand) {
            return new EqualExpression(this, operand);
        }
    }
}
