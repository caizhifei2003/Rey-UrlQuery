﻿using Rey.Exgaming.UrlQuery.Expressions;

namespace Rey.Exgaming.UrlQuery.Operators {
    public class InOperator : UniaryQueryOperator {
        public InOperator() : base(Symbols.In) { }
        public override QueryExpression CreateExpression(QueryOperand operand) {
            return new InExpression(this, operand);
        }
    }
}
