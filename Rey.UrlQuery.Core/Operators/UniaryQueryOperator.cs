﻿namespace Rey.Exgaming.UrlQuery.Operators {
    public abstract class UniaryQueryOperator : QueryOperator {
        public UniaryQueryOperator(string symbol)
            : base(symbol) {
        }

        public abstract QueryExpression CreateExpression(QueryOperand operand);
    }
}
