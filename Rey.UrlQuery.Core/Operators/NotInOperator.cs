﻿using Rey.Exgaming.UrlQuery.Expressions;

namespace Rey.Exgaming.UrlQuery.Operators {
    public class NotInOperator : UniaryQueryOperator {
        public NotInOperator() : base(Symbols.NotIn) { }
        public override QueryExpression CreateExpression(QueryOperand operand) {
            return new NotInExpression(this, operand);
        }
    }
}
