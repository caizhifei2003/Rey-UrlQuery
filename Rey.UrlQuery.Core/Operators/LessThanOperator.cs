﻿using Rey.Exgaming.UrlQuery.Expressions;

namespace Rey.Exgaming.UrlQuery.Operators {
    public class LessThanOperator : UniaryQueryOperator {
        public LessThanOperator() : base(Symbols.LessThan) { }
        public override QueryExpression CreateExpression(QueryOperand operand) {
            return new LessThanExpression(this, operand);
        }
    }
}
