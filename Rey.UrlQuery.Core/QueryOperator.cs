﻿using Rey.Exgaming.UrlQuery.Operators;
using System;

namespace Rey.Exgaming.UrlQuery {
    public abstract class QueryOperator {
        public string Symbol { get; }
        public QueryOperator(string symbol) {
            this.Symbol = symbol;
        }

        public static QueryOperator Create(string symbol) {
            switch (symbol) {
                case Symbols.Equal:
                    return new EqualOperator();
                case Symbols.NotEqual:
                    return new NotEqualOperator();
                case Symbols.GreatThan:
                    return new GreatThanOperator();
                case Symbols.GreatThanEqual:
                    return new GreatThanEqualOperator();
                case Symbols.LessThan:
                    return new LessThanOperator();
                case Symbols.LessThanEqual:
                    return new LessThanEqualOperator();
                case Symbols.In:
                    return new InOperator();
                case Symbols.NotIn:
                    return new NotInOperator();
                case Symbols.And:
                    return new AndOperator();
                case Symbols.Or:
                    return new OrOperator();
                default:
                    throw new InvalidOperationException("cannot find operator");
            }
        }

        public static class Symbols {
            public const string Equal = "eq";
            public const string NotEqual = "ne";
            public const string GreatThan = "gt";
            public const string GreatThanEqual = "gte";
            public const string LessThan = "lt";
            public const string LessThanEqual = "lte";
            public const string In = "in";
            public const string NotIn = "nin";

            public const string And = "and";
            public const string Or = "or";
            public const string Nor = "nor";
            public const string Not = "not";

            public static string[] All { get; } = new string[] {
                Equal,
                NotEqual,
                GreatThan,
                GreatThanEqual,
                LessThan,
                LessThanEqual,
                In,
                NotIn,

                And,
                Or,
                Nor,
                Not
            };
        }
    }
}
