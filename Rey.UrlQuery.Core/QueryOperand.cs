﻿namespace Rey.Exgaming.UrlQuery {
    public class QueryOperand {
        public string Name { get; }
        public string Value { get; }

        public QueryOperand(string name, string value) {
            this.Name = name;
            this.Value = value;
        }
    }
}
