﻿namespace Rey.Exgaming.UrlQuery {
    public class OperandBuilder {
        public char[] Chars { get; }

        public OperandBuilder(char[] chars) {
            this.Chars = chars;
        }

        public QueryOperand Build(ref int index) {
            var ch = this.Chars[index];
            if (!char.IsLetter(ch)) {
                return null;
            }

            var name = "" + ch;
            while (index < this.Chars.Length - 1) {
                ch = this.Chars[index + 1];
                if (ch == '-')
                    break;

                name += ch;
                ++index;
            }

            ++index;

            var value = "";
            while (index < this.Chars.Length - 1) {
                ch = this.Chars[index + 1];
                if (ch == '-')
                    break;

                value += ch;
                ++index;
            }

            return new QueryOperand(name, value);
        }
    }
}
