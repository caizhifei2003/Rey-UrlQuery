﻿using Rey.Exgaming.UrlQuery.Operators;
using System.Collections.Generic;

namespace Rey.Exgaming.UrlQuery {
    //! validable characters: -_~.
    //! ~ 操作符关键字
    //! - 分隔符
    //! . 层级分隔符
    //! _ 转义字符： _~, _-, _., __
    //! operators:
    //! ~eq ~ne ~gt ~gte ~lt ~lte ~in ~nin
    //! ~and ~or ~nor ~not
    //! example 1: ~eq-name-kevin   : name==kevin
    //! example 2: ~and-~eq-name-kevin-~lt-age-24   : ((name==kevin) && (age > 24))
    public class UrlQueryParser {
        public QueryExpression Parse(string content) {
            var opors = new Stack<QueryOperator>();
            var opnds = new Stack<QueryOperand>();

            var opor_prefix = '~';
            var delimiter = '-';

            var chars = content.ToCharArray();
            var index = 0;
            var oporBuilder = new OperatorBuilder(chars);
            var opndBuilder = new OperandBuilder(chars);

            while (index < chars.Length) {
                var opor = oporBuilder.Build(ref index);
                if (opor != null) {
                    opors.Push(opor);
                }

                var opnd = opndBuilder.Build(ref index);
                if (opnd != null) {
                    opnds.Push(opnd);
                }

                ++index;
            }

            var exps = new Stack<QueryExpression>();
            while (opors.Count > 0) {
                var opor = opors.Pop();
                if (opor is UniaryQueryOperator) {
                    var opnd = opnds.Pop();
                    var exp = (opor as UniaryQueryOperator).CreateExpression(opnd);
                    exps.Push(exp);
                } else if (opor is LogicalOperator) {
                    var exp1 = exps.Pop();
                    var exp2 = exps.Pop();
                    var exp = (opor as LogicalOperator).CreateExpression(exp1, exp2);
                    exps.Push(exp);
                }
            }

            return exps.Pop();
        }
    }
}
