﻿namespace Rey.Exgaming.UrlQuery {
    public abstract class QueryExpression {
        public abstract string ToDebugString();
    }
}
