﻿using Rey.Exgaming.UrlQuery.Operators;

namespace Rey.Exgaming.UrlQuery.Expressions {
    public class EqualExpression : ComparisonExpression {
        public EqualExpression(EqualOperator opor, QueryOperand opnd) : base(opor, opnd) {
        }

        public override string ToDebugString() {
            return $"({this.Operand.Name} $eq {this.Operand.Value})";
        }
    }
}
