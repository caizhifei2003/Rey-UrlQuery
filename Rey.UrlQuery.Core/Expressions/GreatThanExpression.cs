﻿using Rey.Exgaming.UrlQuery.Operators;

namespace Rey.Exgaming.UrlQuery.Expressions {
    public class GreatThanExpression : ComparisonExpression {
        public GreatThanExpression(GreatThanOperator opor, QueryOperand opnd) : base(opor, opnd) {
        }

        public override string ToDebugString() {
            return $"({this.Operand.Name} $gt {this.Operand.Value})";
        }
    }
}
