﻿namespace Rey.Exgaming.UrlQuery.Expressions {
    public abstract class LogicalExpression : QueryExpression {
        public QueryOperator Operator { get; }
        public QueryExpression Exp1 { get; }
        public QueryExpression Exp2 { get; }

        public LogicalExpression(QueryOperator opor, QueryExpression exp1, QueryExpression exp2) {
            this.Operator = opor;
            this.Exp1 = exp1;
            this.Exp2 = exp2;
        }
    }
}
