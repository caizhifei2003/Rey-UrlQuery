﻿using Rey.Exgaming.UrlQuery.Operators;

namespace Rey.Exgaming.UrlQuery.Expressions {
    public class NotEqualExpression : ComparisonExpression {
        public NotEqualExpression(NotEqualOperator opor, QueryOperand opnd) : base(opor, opnd) {
        }

        public override string ToDebugString() {
            return $"({this.Operand.Name} $e {this.Operand.Value})";
        }
    }
}
