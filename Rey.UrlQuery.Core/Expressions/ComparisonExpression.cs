﻿namespace Rey.Exgaming.UrlQuery.Expressions {
    public abstract class ComparisonExpression : QueryExpression {
        public QueryOperator Operator { get; }
        public QueryOperand Operand { get; }

        public ComparisonExpression(QueryOperator opor, QueryOperand opnd) {
            this.Operator = opor;
            this.Operand = opnd;
        }
    }
}
