﻿using Rey.Exgaming.UrlQuery.Operators;

namespace Rey.Exgaming.UrlQuery.Expressions {
    public class NotInExpression : ComparisonExpression {
        public NotInExpression(NotInOperator opor, QueryOperand opnd) : base(opor, opnd) {
        }

        public override string ToDebugString() {
            return $"({this.Operand.Name} $nin {this.Operand.Value})";
        }
    }
}
