﻿using Rey.Exgaming.UrlQuery.Operators;

namespace Rey.Exgaming.UrlQuery.Expressions {
    public class LessThanExpression : ComparisonExpression {
        public LessThanExpression(LessThanOperator opor, QueryOperand opnd) : base(opor, opnd) {
        }

        public override string ToDebugString() {
            return $"({this.Operand.Name} $lt {this.Operand.Value})";
        }
    }
}
