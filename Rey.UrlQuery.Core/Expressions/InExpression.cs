﻿using Rey.Exgaming.UrlQuery.Operators;

namespace Rey.Exgaming.UrlQuery.Expressions {
    public class InExpression : ComparisonExpression {
        public InExpression(InOperator opor, QueryOperand opnd) : base(opor, opnd) {
        }

        public override string ToDebugString() {
            return $"({this.Operand.Name} $in {this.Operand.Value})";
        }
    }
}
