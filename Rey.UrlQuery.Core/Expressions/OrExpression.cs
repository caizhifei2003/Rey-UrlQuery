﻿using Rey.Exgaming.UrlQuery.Operators;

namespace Rey.Exgaming.UrlQuery.Expressions {
    public class OrExpression : LogicalExpression {
        public OrExpression(OrOperator opor, QueryExpression exp1, QueryExpression exp2) : base(opor, exp1, exp2) {
        }

        public override string ToDebugString() {
            return $"({this.Exp1.ToDebugString()} $or {this.Exp2.ToDebugString()})";
        }
    }
}
