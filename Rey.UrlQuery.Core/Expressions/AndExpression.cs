﻿using Rey.Exgaming.UrlQuery.Operators;

namespace Rey.Exgaming.UrlQuery.Expressions {
    public class AndExpression : LogicalExpression {
        public AndExpression(AndOperator opor, QueryExpression exp1, QueryExpression exp2) : base(opor, exp1, exp2) {
        }

        public override string ToDebugString() {
            return $"({this.Exp1.ToDebugString()} $and {this.Exp2.ToDebugString()})";
        }
    }
}
