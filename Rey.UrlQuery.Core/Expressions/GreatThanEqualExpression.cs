﻿using Rey.Exgaming.UrlQuery.Operators;

namespace Rey.Exgaming.UrlQuery.Expressions {
    public class GreatThanEqualExpression : ComparisonExpression {
        public GreatThanEqualExpression(GreatThanEqualOperator opor, QueryOperand opnd) : base(opor, opnd) {
        }

        public override string ToDebugString() {
            return $"({this.Operand.Name} $gte {this.Operand.Value})";
        }
    }
}
