﻿using Rey.Exgaming.UrlQuery.Operators;

namespace Rey.Exgaming.UrlQuery.Expressions {
    public class LessThanEqualExpression : ComparisonExpression {
        public LessThanEqualExpression(LessThanEqualOperator opor, QueryOperand opnd) : base(opor, opnd) {
        }

        public override string ToDebugString() {
            return $"({this.Operand.Name} $lte {this.Operand.Value})";
        }
    }
}
